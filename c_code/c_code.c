#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

// The function below will return a NULL-terminated char array
// The code using the return value will have to free its memory
char* function_returning_a_char_pointer() {
    char orig_str[] = "This is a C-style NULL-terminated string";
    char *return_str = strndup(orig_str, strlen(orig_str));
    
    return return_str;
}

// Function which prints the given strings to stdout, then frees them
void function_printing_two_char_arrays(char *string1, char *string2) {
    printf("Printing from C code: <<%s %s>>\n", string1, string2);
}
